class EmailValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    record.errors.add(attribute, (options[:message] || I18n.t(:invalid_email))) unless valid?(value)
  end

  def valid?(value)
    value =~ REGEXPS[:email]
  end
end

class Book < ActiveRecord::Base
  has_many :issued_books, dependent: :destroy
  has_many :user, through: :issued_books

  validates :name, :quantity, presence: true
  validate :copies_issued_cannot_be_greater_than_quantity

  scope :available, -> { where('copies_issued < quantity') }

  def available_copies
    quantity - copies_issued
  end

  private
    def copies_issued_cannot_be_greater_than_quantity
      if copies_issued > quantity
        errors.add(:base, I18n.t(:copies_issued_cannot_be_greater_than_quantity))
      end
    end
end

class Role < ActiveRecord::Base
  include RoleFinder

  validates :name, presence: true
  validates :name, uniqueness: { allow_blank: true, case_sensitive: false }

  has_many :users, dependent: :restrict_with_error
end

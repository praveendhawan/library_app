class User < ActiveRecord::Base
  has_secure_password

  include RoleHelpers

  belongs_to :role
  has_many :issued_books, dependent: :destroy
  has_many :books, through: :issued_books

  validates :email, :password, :library_id, presence: true
  validates :email, email: true
  validates :password, length: { in: 8..10 }

  before_validation :assign_library_id, if: :new_record?

  private

    def assign_library_id
      self.library_id = "MYLIB#{ 100 + User.count + rand(100) }"
    end
end

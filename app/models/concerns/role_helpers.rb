module RoleHelpers
  extend ActiveSupport::Concern

  included do
    define_role_validators
  end

  module ClassMethods

    def define_role_validators
      Role.pluck(:name).each do |name|
        define_method "#{name}?" do
          role == Role.find_by(name: name)
        end
      end
    end

  end
end

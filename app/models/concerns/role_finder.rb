module RoleFinder
  extend ActiveSupport::Concern

  included do
    define_role_finders
  end

  module ClassMethods

    def define_role_finders
      class << self
        Role.pluck(:name).each do |name|
          define_method "#{name}" do
            find_by(name: name)
          end
        end
      end
    end

  end
end

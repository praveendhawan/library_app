class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  authorize_resource
  before_action :authorize
  helper_method :current_user

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { render nothing: true, status: 403 }
      format.html { redirect_to root_url, flash: { error: t(:access_denied) } }
    end
  end

  private

    def current_user
      @current_user ||= User.find_by(id: session[:user_id])
    end

    def authorize
      redirect_to login_url, alert: t(:access_denied) unless current_user
    end
end

class SessionsController < ApplicationController
  before_action :reset_session, only: [:create, :destroy]
  before_action :set_user, only: :create

  skip_before_action :authorize
  skip_authorize_resource

  def new; end

  def create
    if @user and @user.authenticate(params[:password])
      session[:user_id] = @user.id
      redirect_to root_url, notice: t(:login_successful)
    else
      redirect_to login_url, alert: t(:invalid_credentials)
    end
  end

  def destroy
    redirect_to root_url, notice: t(:logout_successful)
  end

  private

    def set_user
      @user = User.find_by(email: params[:email])
    end

end

require 'rails_helper'

RSpec.describe Book, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:issued_books).dependent(:destroy) }
    it { is_expected.to have_many(:user).through(:issued_books) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:quantity) }
  end

  describe 'scopes' do
  end

  describe '#available_copies' do
  end
end

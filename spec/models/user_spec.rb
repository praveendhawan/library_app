require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to have_secure_password }

  describe 'associations' do
    it { is_expected.to belong_to :role }
    it { is_expected.to have_many(:issued_books).dependent(:destroy) }
    it { is_expected.to have_many(:books).through(:issued_books) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:password) }
    xit { is_expected.to validate_presence_of(:library_id) }
    it { is_expected.to validate_length_of(:password).is_at_least(8).is_at_most(10) }
  end

  describe 'callbacks' do
    it { is_expected.to callback(:assign_library_id).before(:validation) }
  end

  describe '#assign_library_id' do
  end
end

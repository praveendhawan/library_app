FactoryGirl.define do
  factory :book do
    name 'test name of book'
    quantity 10
    copies_issued 6
  end
end

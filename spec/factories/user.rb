FactoryGirl.define do
  factory :user do
    name 'test name for user'
    email 'test@test.com'
    password 'dummyPass'
    association :role, factory: :normal_role
  end
end

FactoryGirl.define do
  factory :admin_role, class: Roles do
    name 'admin'
  end

  factory :normal_role, class: Roles do
    name 'normal'
  end
end

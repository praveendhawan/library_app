class CreateIssuedBooks < ActiveRecord::Migration
  def change
    create_table :issued_books do |t|
      t.integer :book_id
      t.integer :user_id
    end
    add_index :issued_books, :book_id
    add_index :issued_books, :user_id
  end
end

class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :name
      t.integer :quantity
      t.integer :copies_issued

      t.timestamps null: false
    end
  end
end

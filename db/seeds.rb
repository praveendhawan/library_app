# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Role.delete_all
Role.create([{ name: 'admin' }, { name: 'normal' }])

Book.delete_all
Book.create([{ name: 'The Jungle Book', quantity: 10, copies_issued: 6 },
  { name: 'War and Peace', quantity: 2, copies_issued: 1 },
  { name: 'Da Vinci Code', quantity: 5, copies_issued: 0 }])

User.delete_all
User.create([
  { name: 'Dean', library_id: 'MYLIB100', role_id: Role.find_by(name: 'admin').id,
    email: 'dean@example.com', password: 'qwertyuiop' },
  { name: 'Sam Winchestor', library_id: 'MYLIB101', role_id: Role.find_by(name: 'normal').id,
    email: 'sam@example.com', password: 'qwertyuiop' },
  { name: 'Indiana Jones', library_id: 'MYLIB102', role_id: Role.find_by(name: 'normal').id,
    email: 'jones@example.com', password: 'qwertyuiop' }
  ])

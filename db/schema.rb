# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160619145922) do

  create_table "books", force: :cascade do |t|
    t.string   "name"
    t.integer  "quantity"
    t.integer  "copies_issued"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "issued_books", force: :cascade do |t|
    t.integer "book_id"
    t.integer "user_id"
  end

  add_index "issued_books", ["book_id"], name: "index_issued_books_on_book_id"
  add_index "issued_books", ["user_id"], name: "index_issued_books_on_user_id"

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "library_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "role_id"
    t.string   "email"
    t.string   "password_digest"
  end

end
